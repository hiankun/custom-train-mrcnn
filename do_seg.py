import os
import sys
import numpy as np
import cv2
from train import CustomDataset, CustomConfig
from pathlib import Path

# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
from mrcnn.model import log

MODEL_DIR = os.path.join(ROOT_DIR, "logs")


class InferenceConfig(CustomConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

def show_splash(image, masks):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.merge((gray,gray,gray))
    if masks.shape[-1] > 0:
        masks = (np.sum(masks, -1, keepdims=True) >=1 )
        splash = np.where(masks, image, gray).astype(np.uint8)
    else:
        splash = gray.astype(np.uint8)

    return splash

def show_masks(image, masks):
    res_img = image
    masks = np.rollaxis(masks, -1)
    for _mask in masks:
        m = np.where(_mask, 1, 0)
        _mask = cv2.merge((m,m,m))
        _mask = np.where(_mask >0, (0,255,255), (0,0,0))
        mask = np.asarray(_mask, dtype=np.uint8)
        cv2.addWeighted(mask, 0.5, image, 1.0, 0, res_img)

    return res_img

def show_results(image, results):
    res_img = image
    r = results[0]
    rois = r['rois']
    class_ids = r['class_ids']
    scores = r['scores']
    masks = np.rollaxis(r['masks'], -1)

    for i in range(len(class_ids)):
        roi = rois[i]
        class_id = class_ids[i]
        score = scores[i]
        mask = masks[i]

        m = np.where(mask, class_id, 0)
        _mask = cv2.merge((m,m,m))
        _mask = np.where(_mask >0, (0,255,255), (0,0,0))
        mask = np.asarray(_mask, dtype=np.uint8)
        cv2.addWeighted(mask, 0.5, image, 1.0, 0, res_img)
        cv2.rectangle(res_img, (roi[1],roi[0]), (roi[3],roi[2]), (0,255,255), 2)
        #_score = f'{score:.2f}'
        #cv2.putText(res_img, _score, (roi[1],roi[0]), 1, 1, (255,255,255), 2)

    return res_img

def main():
    data_set = "aqua"
    inference_config = InferenceConfig()
    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                              config=inference_config,
                              model_dir=MODEL_DIR)
    cv2.namedWindow("Mask R-CNN", cv2.WINDOW_NORMAL)
    
    # Get path to saved weights
    # Either set a specific path or find last trained weights
    # model_path = os.path.join(ROOT_DIR, ".h5 file name here")

    #model_path = model.find_last()
    #model_path = './logs/aqua20200725T2128/mask_rcnn_aqua_0090.h5'
    #model_path = './logs/aqua20200728T0953/mask_rcnn_aqua_0019.h5'
    #model_path = './logs/aqua20200729T1833/mask_rcnn_aqua_0019.h5'
    #model_path = './logs/aqua20200729T2303/mask_rcnn_aqua_0030.h5' #tetra: seems work
    #model_path = './logs/aqua20200729T2354/mask_rcnn_aqua_0048.h5'
    #model_path = './logs/aqua20200730T2110/mask_rcnn_aqua_0018.h5'
    #model_path = './logs/aqua20201015T0030/mask_rcnn_aqua_0100.h5' #shrimp: ng
    #model_path = './logs/aqua20201018T1736/mask_rcnn_aqua_0030.h5' #tetra: ng
    #model_path = './logs/aqua20201018T2219/mask_rcnn_aqua_0060.h5' #tetra: ok
    model_path = './logs/aqua20201019T0033/mask_rcnn_aqua_0060.h5' #shrimp: ok
    
    # Load trained weights
    print("Loading weights from ", model_path)
    model.load_weights(model_path, by_name=True)
    
    #cap = cv2.VideoCapture('/home/thk/Downloads/MOV_0880.mp4')
    #cap = cv2.VideoCapture('/home/thk/Downloads/MOV_0862.mp4')
    #cap = cv2.VideoCapture('/home/thk/Downloads/MOV_0849.mp4')
    #cap = cv2.VideoCapture('/media/thk/transcend_1T/DataSet/AquaData/shrimp/test_vid/test001.mkv')
    #cap = cv2.VideoCapture('/media/thk/transcend_1T/DataSet/AquaData/shrimp/test_vid/test002.mkv')

    #for img_f in Path('aqua/test/').glob('*.jpg'):
    ##for img_f in Path('/home/thk/').glob('mpv-*.jpg'):
    #for img_f in Path('/media/thk/transcend_1T/DataSet/AquaData/tetra_shrimp/test_pic/').glob('*.jpg'):
    for img_f in Path('/media/thk/transcend_1T/DataSet/AquaData/shrimp/test_pic/').glob('*.jpg'):
        test_img = cv2.imread(str(img_f))
    #while True:
    #    _, test_img = cap.read()
        # Do detection
        results = model.detect([test_img], verbose=0)
        #res_img = show_masks(test_img, results[0]['masks'])
        res_img = show_results(test_img, results)
        #res_img_f = f'{img_f.parent/img_f.stem}_res.jpg'
        #cv2.imwrite(res_img_f, res_img)
        cv2.imshow("Mask R-CNN", res_img)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()


if __name__=='__main__':
    main()
