rm train/images/*
rm train/png_masks/*
rm val/images/*
rm val/png_masks/*

# [2020-09-13] The following path setting has not been tested.
#dataset_path="/media/thk/backup_plus/DataSet/mrcnn_tetra_test"
dataset_path="/media/thk/transcend_1T/DataSet/AquaData/tetra_shrimp/mrcnn_tetra_test/"
ln -s ${dataset_path}/dataset_5/images/scape[0-9][0-7]*.jpg train/images/
ln -s ${dataset_path}/dataset_5/images/scape[0-9][8-9]*.jpg val/images/
ln -s ${dataset_path}/dataset_5/png_masks/scape[0-9][0-7]*.png train/png_masks/
ln -s ${dataset_path}/dataset_5/png_masks/scape[0-9][8-9]*.png val/png_masks/
