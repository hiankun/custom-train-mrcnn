[2020-07-25]

* GPU: GTX 1080
* Using the syntesis tetra data set for training.
* All the images and PNG masks were just soft link to the original dataset.
* The training was completed within 31 minutes, and the validation looked good (too good to be true...).
* TODO: To write a test script for testing the trained model on other images or videos.

[2020-07-26]

* Test the model of `logs/aqua20200725T2128/mask_rcnn_aqua_0030.h5` using splash functions
  as a quick check. The result is as follows:

  ![](pics/0725_splash/test_res_01.jpg)
  ![](pics/0725_splash/test_res_02.jpg)
  ![](pics/0725_splash/test_res_03.jpg)
  ![](pics/0725_splash/test_res_04.jpg)
  ![](pics/0725_splash/test_res_05.jpg)
  ![](pics/0725_splash/test_res_06.jpg)


[2020-07-28]
# Try augmented dataset

* Add overlap checking to prevent objects occlusion.
* Use 3 different base size (100, 90, and 80 px) to generate 3 sets for training.
* Flip the images and the masks to double the dataset.
* Therefore, there are `50*3*2 = 300` images in the dataset.
* Use the following command to link to the real image and masks:
  ```
  ln -s /media/thk/workspace/gitlab_proj/itri_sandbox/seg_aug/images/scape[0-9][0-7]_*.jpg train/images/
  ln -s /media/thk/workspace/gitlab_proj/itri_sandbox/seg_aug/images/scape[0-9][8-9]_*.jpg val/images/
  ln -s /media/thk/workspace/gitlab_proj/itri_sandbox/seg_aug/png_masks/scape[0-9][0-7]_*.png train/png_masks/
  ln -s /media/thk/workspace/gitlab_proj/itri_sandbox/seg_aug/png_masks/scape[0-9][8-9]_*.png val/png_masks/
  ```
* According to warning messages, modified relevant code to prevent the messy 
  text messages when training.
* Keep to train the model to step 300, and its loss still keeps going down.
  However, the model detects less objects.

# Results comparison
[2020-07-29]

## Model 1
* Dataset: 50 aquascapes with randomly positioned tetra fishes.
  * `/media/thk/backup_plus/DataSet/mrcnn_tetra_test/dataset_2/`
* Overlap: Yes
* Keep aspect ratio: Yes
* Flip: Yes
* Total images: 50
* Model: `../logs/aqua20200725T2128/mask_rcnn_aqua_0030.h5`
* Results:

  ![](pics/0725/test_res_01.jpg)
  ![](pics/0725/test_res_02.jpg)
  ![](pics/0725/test_res_03.jpg)
  ![](pics/0725/test_res_04.jpg)
  ![](pics/0725/test_res_05.jpg)
  ![](pics/0725/test_res_06.jpg)


## Model 2-0030
* Dataset: 50 aquascapes with randomly positioned tetra fishes.
  * `/media/thk/backup_plus/DataSet/mrcnn_tetra_test/dataset_2/`
* Overlap: No
* Keep aspect ratio: No
* Flip: Yes
* Total images: 300
* Model: `../logs/aqua20200728T0953/mask_rcnn_aqua_0030.h5`
* Results: Seems slightly better than Model 1 (less false positive)

  ![](pics/0728_0030/test_res_01.jpg)
  ![](pics/0728_0030/test_res_02.jpg)
  ![](pics/0728_0030/test_res_03.jpg)
  ![](pics/0728_0030/test_res_04.jpg)
  ![](pics/0728_0030/test_res_05.jpg)
  ![](pics/0728_0030/test_res_06.jpg)

## Model 2-0300
* Model: `../logs/aqua20200728T0953/mask_rcnn_aqua_0300.h5`
* Results: Worse than Model 1 and Model 2-0030

  ![](pics/0728_0300/test_res_01.jpg)
  ![](pics/0728_0300/test_res_02.jpg)
  ![](pics/0728_0300/test_res_03.jpg)
  ![](pics/0728_0300/test_res_04.jpg)
  ![](pics/0728_0300/test_res_05.jpg)
  ![](pics/0728_0300/test_res_06.jpg)

## Model 3-0090
* Dataset: `/media/thk/backup_plus/DataSet/mrcnn_tetra_test/dataset_3/`
* Model: `../logs/aqua20200729T1833/mask_rcnn_aqua_0090.h5'
* Results: No improvement.
* The train/val losses are as follows:

  ![](pics/0729_0090/train_loss.png)
  ![](pics/0729_0090/val_loss.png)

  They are quite the same as the example of __Unrepresentative Validation Dataset__
  shown in ref 1.

---

[2020-10-13]

* Create the shrimp dataset (21,900 pairs) in 
  `/media/thk/transcend_2T/DataSet/AquaData/shrimp/dataset/`.
* Under it, create `train_20200113/` and link images/masks to corresponding 
  folders for mrcnn training:
  ```
  train_20200113/
  ├── train
  │   ├── images
  │   └── png_masks
  └── val
      ├── images
      └── png_masks
  ```
  * train set: `scape_0[0-6]*.*`
  * val set: `scape_07*.*`

---

[1] [How to use Learning Curves to Diagnose Machine Learning Model Performance](
https://machinelearningmastery.com/learning-curves-for-diagnosing-machine-learning-model-performance/)
