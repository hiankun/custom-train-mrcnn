# Train multiple classes in Mask R-CNN

The code base was from [miki998/Custom_Train_MaskRCNN](https://github.com/miki998/Custom_Train_MaskRCNN),
and some parts were based on [the shapes sample](https://github.com/matterport/Mask_RCNN/tree/master/samples/shapes) in the [matterport/Mask_RCNN](https://github.com/matterport/Mask_RCNN) repo.

I modified the code to fit my own need.
It can use polygons or PNG masks as the input annotations to train the shapes sample.
However, the code and the working processes are totally a mess now.
I will find some time to re-test all the processes again 
and write down the overall working steps in the following weeks.
