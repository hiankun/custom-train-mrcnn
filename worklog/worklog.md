```
conda create -n tf_mrcnn_env -c conda-forge tensorflow-gpu=1.14
conda install -c conda-forge jupyter pycocotools scikit-image keras imgaug
```

[2020-04-29]

* Cloned from [miki998/Custom_Train_MaskRCNN](https://github.com/miki998/Custom_Train_MaskRCNN)

* Create `shapes` dataset in `/media/thk/workspace/DataSet/shapes`.
  * Use `tools/contour2poly.py` to create annotations from the masks to csv files.
  * TODO: to detect the shape's name and then assign it to the "label" value.
  * TODO: to save the print strings into annotation csv files.

[2020-05-02]

* Run training:
  `python train.py train --dataset=./shapes --weights=coco`

* Continue training:
  `python train.py train --dataset=./shapes --weights=last`
  * NOTE: Remeber to increase `epochs` in `train()`.

* Training mode:
  * Begin with `layers='heads'` using larger lr;
  * then `layers='all'` using smaller lr.
  * __NOTE__: the above method didn't get better results.

[2020-06-24]

* Re-visit this repo. 

## Train
```
bash run.sh
```

## Test
```
python check_shapes_train_results.py
```

TODO: to prepare aqua set...

[2020-06-26]

* Try to run the "shapes dataset preparation" again.
* The code is in 
  `/media/thk/workspace/gitlab_proj/tf_mrcnn_test/samples/test_shapes/train_shapes.ipynb`.
  * When running the notebook, it complains about "no keras module".
  * Turns out that the notebook uses another kernel from 
    `/media/thk/workspace/github_proj/handson-ml2/`. 
    We can check the path by using `sys.path` in the notebook.
  * The solution is to install `nb_conda` which enable the user to change kernels from 
    within the jupyter environment.
    (ref:[issues/3311](https://github.com/jupyter/notebook/issues/3311#issuecomment-363804324))
  
    ![](pics/nb_conda-kernel.png)
 
# Re-think the input mask format
[2020-06-30]

In the TensorFlow document [Run an Instance Segmentation Model](
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/instance_segmentation.md)
it reads:

> PNG masks are the preferred parameterization since they offer considerable 
> space savings compared to dense numerical masks.

Also, when converting the mask data to TFRecords, PNG masks seem to be necessary.

Another reason to use PNG masks instead of polygons is that PNG masks can handle
"holes in the mask" directly.

[2020-07-01]

* In `load_mask()`, changed the assigned values for `image_id`.
* Use `../shapes/tools/mask_poly2png.py` to generate PNG masks (saved in `png_masks` folders).
  * Note that every instance should be bordered with value 255 to avoid adjacent
    instances merging to single instance.
  * ref: https://stackoverflow.com/questions/49629933/ground-truth-pixel-labels-in-pascal-voc-for-semantic-segmentation/61425574#61425574
* Try to use `../shapes/tools/load_png_masks.py` to make sure how to feed PNG masks in to the training process...
  * For each mask image, try to generate `class_ids` and corresponding masks.
  * Extract the `rr, cc` from the PNG masks.

[2020-07-06]

* Complete training with PNG masks.
* Some TODOs in the code...

[2020-07-28]

* Downgrade `scikit-image` from `0.17.2` to `0.16.2` to prevent warnings.
* (Don't do this. See [2020-07-29]) <s>Modify code:</s>
  ```
  /home/thk/miniconda3/envs/mrcnn_env/lib/python3.7/site-packages/keras/callbacks/tensorboard_v1.py:203: 
  The name tf.summary.FileWriter is deprecated. Please use tf.compat.v1.summary.FileWriter instead.
  
  /home/thk/miniconda3/envs/mrcnn_env/lib/python3.7/site-packages/keras/callbacks/tensorboard_v1.py:343:
  The name tf.Summary is deprecated. Please use tf.compat.v1.Summary instead.
  ```

[2020-07-29]

* To prevent `compat.v1` warnings, use the following code 
([ref](https://github.com/tensorflow/tensorflow/issues/27023#issuecomment-523710266)):
  ```
  import tensorflow as tf
  tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
  ```

