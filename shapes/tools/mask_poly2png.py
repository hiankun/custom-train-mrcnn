# Convert polygon annotations to PNG masks

from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
from pathlib import Path
import csv

DEBUG = False
annos_path = "../val/annos/"
masks_path = "../val/png_masks/"
labels_file = "../val/labels"


def get_labels(labels_f):
    with open(labels_f) as f:
        labels = f.read().splitlines()
    return labels


def main():
    labels = get_labels(labels_file)

    field_names = ('filename','width','height','class_name')
    for anno_f in Path(annos_path).glob('*.csv'):
        with open(anno_f) as csvfile:
            reader = csv.DictReader(csvfile, field_names, restkey='polygon')
            mask_img = Image.new('L', (128,128), 0)
            for row in reader:
                filename = row['filename']
                class_name = row['class_name']
                polygon = [int(p) for p in row['polygon']]

                # Draw masks
                mask_value = labels.index(class_name) + 1 # 0:BG
                mask_draw = ImageDraw.Draw(mask_img)
                mask_draw.polygon(polygon, fill=mask_value, outline=255)
                # Save masks
                mask_f = str(Path(masks_path)/Path(filename).stem)+'.png'
                mask_img.save(mask_f)
                print(f'{mask_f} has been saved...')

            if DEBUG:
                plt.imshow(mask_img, cmap='flag')
                plt.show()



if __name__=='__main__':
    main()
