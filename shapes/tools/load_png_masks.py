#import matplotlib.pyplot as plt
import numpy as np
import skimage.io as skio
import skimage.measure as skm
from pathlib import Path

#TODO: 2 val png_masks show bugs (030, 033), to check them...
# check the definition of "connectivity"...

# in load_custom(), add mask image path, its shape info and the class_ids.
png_masks = Path('../val/png_masks/')
for mask_f in png_masks.glob('*.png'):
    mask_img = skio.imread(mask_f)
    print(mask_f, mask_img.shape) #TODO: use this to get image (height, width)
    parent_folder = Path(mask_f).resolve().parents[1]
    image_name = str(mask_f.stem)+'.jpg'
    image_path = parent_folder/'images'/image_name
    print(image_path.is_file())
    
    # in load_mask(), do the following conversion.
    mask_instances = []
    class_ids = []
    for mask_id in range(1, 4):
        # extract mask regions with the same class id
        tmp_mask = np.zeros_like(mask_img)
        rr, cc = np.where(mask_img == mask_id) #TODO: use this line in load_mask()
        if rr.size > 0:
            tmp_mask[rr, cc] = mask_id #* 50
    
            # lable the instance(s) of the same class regions
            label_mask = skm.label(tmp_mask, connectivity=1)#tmp_mask.ndim)
    
            #>>> debug
            #skio.imshow(tmp_mask)
            #skio.show()
            #skio.imshow(label_mask)
            #skio.show()
            #<<< debug
    
            label_ids= []
            props = skm.regionprops(label_mask)
            for prop in props:
                #print(f'mask_id: {mask_id}, label_id: {prop["label"]}')
                label_ids.append(prop["label"])
                class_ids.append(mask_id)
    
            print(f'mask_id: {mask_id}, label_ids: {label_ids}')
            for label_id in label_ids:
                mask_instance = np.zeros_like(mask_img)
                rr, cc = np.where(label_mask == label_id)
                mask_instance[rr, cc] = 1
                mask_instances.append(mask_instance)
    mask_instances = np.stack(mask_instances, axis=2)
    print('xxxxxx', mask_instances.shape)
    print(f'class_ids: {class_ids}')
    print(f'mask_instances: {len(mask_instances)}')
    
    skio.imshow(mask_img)
    skio.show()
