import numpy as np
from PIL import Image
from glob import glob
from collections import Counter

files = glob('../val/masks/*.png')
all_list = []
for f in files:
    annotation = np.array(Image.open(f))
    #print(annotation[200,200])
    print(Counter(annotation.flatten()))
    all_list.extend(set(annotation.flatten()))
    img = Image.open(f)
    #print(img)
    #img.show()
print(set(all_list))
