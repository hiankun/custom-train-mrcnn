# VGG Image Annotator saves each image in the form:
# { 'filename': '28503151_5b5b7ec140_b.jpg',
#   'regions': {
#       '0': {
#           'region_attributes': {},
#           'shape_attributes': {
#               'all_points_x': [...],
#               'all_points_y': [...],
#               'name': 'polygon'}},
#       ... more regions ...
#   },
#   'size': 100202
# }
# We mostly care about the x and y coordinates of each region

import csv
import json
from pathlib import Path

def main():
    field_names = ('filename','width','height','class_name')
    for anno_f in Path('../val').resolve().glob('annos/*.csv'):
        with open(anno_f) as csvfile:
            reader = csv.DictReader(csvfile, field_names, restkey='polygons')
            for row in reader:
                path = row['filename']
                image_id = Path(path).name
                width = row['width']
                height = row['height']
                class_name = row['class_name']
                polygons = row['polygons']
                all_points_x = [int(pt) for pt in polygons[::2]]
                all_points_y = [int(pt) for pt in polygons[1::2]]
                print(path, image_id, class_name, width, height, 
                        all_points_x, all_points_y)

if __name__=='__main__':
    main()
