import json
from pathlib import Path

jfile = './tuna.json'
annotations = json.load(open(jfile))
annotations = list(annotations.values())
for a in annotations:
    image_id = a["filename"]
    path = Path(image_id).resolve() #fake, need to change
    polygons = [r['shape_attributes'] for r in a['regions']]
    print(f'image_id={image_id}\npath={path}\n'
          f'polygons={polygons}')
