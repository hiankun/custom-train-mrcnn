# This script is used to generate the Shapes dataset within `mrcnn_env`.
# After running `python gen_shapes.py`, there will be a `train` and a `val` folders
# containing generated images with corresponding masks.
# NOTE: The script is just a quick extraction from the original sample code of Mask R-CNN.
#       There might be many unnecessary parts in this script.

import os
import sys
import random
import math
import numpy as np
import cv2

# Import Mask RCNN
ROOT_DIR = os.path.abspath("../../")
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils

class Settings:
    width = 128
    height = 128
    train_sample = 500
    val_sample = 50

class ShapesDataset(utils.Dataset):
    """Generates the shapes synthetic dataset. The dataset consists of simple
    shapes (triangles, squares, circles) placed randomly on a blank surface.
    The images are generated on the fly. No file access required.
    """

    def load_shapes(self, count, height, width):
        """Generate the requested number of synthetic images.
        count: number of images to generate.
        height, width: the size of the generated images.
        """
        # Add classes
        self.add_class("shapes", 1, "square")
        self.add_class("shapes", 2, "circle")
        self.add_class("shapes", 3, "triangle")

        # Add images
        # Generate random specifications of images (i.e. color and
        # list of shapes sizes and locations). This is more compact than
        # actual images. Images are generated on the fly in load_image().
        for i in range(count):
            bg_color, shapes = self.random_image(height, width)
            self.add_image("shapes", image_id=i, path=None,
                           width=width, height=height,
                           bg_color=bg_color, shapes=shapes)

    def load_image(self, image_id):
        """Generate an image from the specs of the given image ID.
        Typically this function loads the image from a file, but
        in this case it generates the image on the fly from the
        specs in image_info.
        """
        info = self.image_info[image_id]
        bg_color = np.array(info['bg_color']).reshape([1, 1, 3])
        image = np.ones([info['height'], info['width'], 3], dtype=np.uint8)
        image = image * bg_color.astype(np.uint8)
        for shape, color, dims in info['shapes']:
            image = self.draw_shape(image, shape, dims, color)
        return image

    def image_reference(self, image_id):
        """Return the shapes data of the image."""
        info = self.image_info[image_id]
        if info["source"] == "shapes":
            return info["shapes"]
        else:
            super(self.__class__).image_reference(self, image_id)

    def load_mask(self, image_id):
        """Generate instance masks for shapes of the given image ID.
        """
        info = self.image_info[image_id]
        shapes = info['shapes']
        count = len(shapes)
        mask = np.zeros([info['height'], info['width'], count], dtype=np.uint8)
        for i, (shape, _, dims) in enumerate(info['shapes']):
            mask[:, :, i:i+1] = self.draw_shape(mask[:, :, i:i+1].copy(),
                                                shape, dims, 1)
        # Handle occlusions
        occlusion = np.logical_not(mask[:, :, -1]).astype(np.uint8)
        for i in range(count-2, -1, -1):
            mask[:, :, i] = mask[:, :, i] * occlusion
            occlusion = np.logical_and(occlusion, np.logical_not(mask[:, :, i]))
        # Map class names to class IDs.
        class_ids = np.array([self.class_names.index(s[0]) for s in shapes])
        
        # load mask PNG files
#        for f in glob(''):
#            m = skimage.io.imread(f).astype(np.bool)
#            mask.append(m)
#        mask = np.stack(mask, axis=-1)
        return mask.astype(np.bool), class_ids.astype(np.int32)

    def draw_shape(self, image, shape, dims, color):
        """Draws a shape from the given specs."""
        # Get the center x, y and the size s
        x, y, s = dims
        if shape == 'square':
            cv2.rectangle(image, (x-s, y-s), (x+s, y+s), color, -1)
        elif shape == "circle":
            cv2.circle(image, (x, y), s, color, -1)
        elif shape == "triangle":
            points = np.array([[(x, y-s),
                                (x-s/math.sin(math.radians(60)), y+s),
                                (x+s/math.sin(math.radians(60)), y+s),
                                ]], dtype=np.int32)
            cv2.fillPoly(image, points, color)
        return image

    def random_shape(self, height, width):
        """Generates specifications of a random shape that lies within
        the given height and width boundaries.
        Returns a tuple of three valus:
        * The shape name (square, circle, ...)
        * Shape color: a tuple of 3 values, RGB.
        * Shape dimensions: A tuple of values that define the shape size
                            and location. Differs per shape type.
        """
        # Shape
        shape = random.choice(["square", "circle", "triangle"])
        # Color
        color = tuple([random.randint(0, 255) for _ in range(3)])
        # Center x, y
        buffer = 20
        y = random.randint(buffer, height - buffer - 1)
        x = random.randint(buffer, width - buffer - 1)
        # Size
        s = random.randint(buffer, height//4)
        return shape, color, (x, y, s)

    def random_image(self, height, width):
        """Creates random specifications of an image with multiple shapes.
        Returns the background color of the image and a list of shape
        specifications that can be used to draw the image.
        """
        # Pick random background color
        bg_color = np.array([random.randint(0, 255) for _ in range(3)])
        # Generate a few random shapes and record their
        # bounding boxes
        shapes = []
        boxes = []
        N = random.randint(1, 4)
        for _ in range(N):
            shape, color, dims = self.random_shape(height, width)
            shapes.append((shape, color, dims))
            x, y, s = dims
            boxes.append([y-s, x-s, y+s, x+s])
        # Apply non-max suppression wit 0.3 threshold to avoid
        # shapes covering each other
        keep_ixs = utils.non_max_suppression(np.array(boxes), np.arange(N), 0.3)
        shapes = [s for i, s in enumerate(shapes) if i in keep_ixs]
        return bg_color, shapes


def generate_masks(mask, class_ids):
    m = mask[:, :, np.where(class_ids >0)[0]]
    gray_scale = 50 # the gray "steps": 0, 50, 100, 150,...
    m = np.sum(m * np.arange(1, m.shape[-1] + 1)*gray_scale, axis=-1)
    return m

def save_generated_datasets(dataset, set_name=None):
    if set_name:
        IMG_SAVE_PATH = os.path.join(os.getcwd(), set_name, 'images')
        MSK_SAVE_PATH = os.path.join(os.getcwd(), set_name, 'masks')
    else:
        print('Please set set_name...')
        return
    
    if not os.path.exists(IMG_SAVE_PATH):
        os.makedirs(IMG_SAVE_PATH)
    if not os.path.exists(MSK_SAVE_PATH):
        os.makedirs(MSK_SAVE_PATH)
        
    for image_id in dataset.image_ids:
        image = dataset.load_image(image_id)
        image_name = os.path.join(
                IMG_SAVE_PATH, '{}-{:03d}.jpg'.format(set_name, image_id))
        mask, class_ids = dataset.load_mask(image_id)
        mask_name = os.path.join(
                MSK_SAVE_PATH, '{}-{:03d}.png'.format(set_name, image_id))
        cv2.imwrite(image_name, image)
        m = generate_masks(mask, class_ids)
        cv2.imwrite(mask_name, m)


# Prepare training dataset
dataset_train = ShapesDataset()
dataset_train.load_shapes(Settings.train_sample, Settings.height, Settings.width)
dataset_train.prepare()

# Prepare validation dataset
dataset_val = ShapesDataset()
dataset_val.load_shapes(Settings.val_sample, Settings.height, Settings.width)
dataset_val.prepare()

# Generate and save the train/val dataset
save_generated_datasets(dataset_train, set_name='train')
save_generated_datasets(dataset_val, set_name='val')
