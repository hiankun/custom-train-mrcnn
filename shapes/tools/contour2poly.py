# usage: python contour2poly.py -m ../val/masks/

import cv2
import numpy as np
from collections import Counter
#from glob import iglob
from pathlib import Path
import argparse
import sys


def cal_angle(pts):
    a,b,c = pts
    ba = a - b
    bc = c - b
    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    return np.arccos(cosine_angle) * 180/np.pi


def estimate_shape_name(pts):
    """ This method use the longest side to caculate the reference angle.
    It will failed when a circle is cropped by a square or a triangle,
    because the reference angle will be 90 or 60 degrees, respectively.
    """
    # expand [pt0, ..., ptn] to [ptn, pt0, ..., ptn, pt0]
    _pts = np.insert(pts, 0, pts[-1], axis=0)
    _pts = np.append(_pts, [pts[0]], axis=0)
    i_dist = 0
    dist = 0
    for i, pt in enumerate(pts):
        d = np.linalg.norm(_pts[i] - _pts[i+1])
        if d > dist:
            i_dist = i
            dist = d
    corner_pts = _pts[i_dist:i_dist+3]
    ang = cal_angle(corner_pts)
    tol = 5.0
    if np.abs(ang - 60.0) < tol:
        return 'triangle'
    elif np.abs(ang - 90.0) < tol:
        return 'square'
    else:
        return 'circle'


def get_shape_name(pts):
    if len(pts) == 3:
        return 'triangle'
    else:
        return estimate_shape_name(pts)


def get_polygon(mask_file, show_img=False):
    polygons = []
    label_str = 'unknown'
    mask = cv2.imread(str(mask_file), cv2.IMREAD_GRAYSCALE)
    # Get mask values except for zero (background)
    mask_values = sorted(set(Counter(mask.flatten())))[1:]
    if show_img:
        bw0 = np.zeros_like(mask)
    for i, m_value in enumerate(mask_values):
        bw = np.where(mask==m_value, 255, 0).astype(np.uint8)
        cnts, _ = cv2.findContours(bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for cnt in cnts:
            epsilon = 0.01*cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, epsilon, True)
            approx = np.squeeze(approx, axis=1)
            label_str = get_shape_name(approx)
            polygons.append((label_str, approx.flatten()))
        if show_img:
            cv2.drawContours(bw, [approx], 0, (175), 2)
            for _, pt in enumerate(approx):
                #cv2.circle(bw, tuple(pt[0]), 3, (100), 2)
                cv2.circle(bw, tuple(pt), 3, (100), 2)
                if _ == 0:
                    cv2.putText(bw, label_str, tuple(pt), 1, 1, (150))
            bw[:, -1:]=255
            if i == 0:
                bw0 = bw
                continue
            bw0 = np.hstack((bw0, bw))
    if show_img:
        mask[:, -1:]=255
        cv2.imshow('poly', np.hstack((mask, bw0)))
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return mask.shape, polygons


def get_annos(fname, size, polygons):
    # NOTE: the fname will be changed to pointing to the jpg images
    img_fname = Path.joinpath(
            Path(fname).parent.parent, 'images', Path(fname).stem+'.jpg')
    w, h = size
    annos = []
    for label, poly in polygons:
        pts = ','.join(map(str,poly))
        annos.append(f'{img_fname},{w},{h},{label},{pts}')
    return annos


def get_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mask_folder', required=True,
            help='folder of PNG mask files')
    args = parser.parse_args()
    return args


def main():
    args = get_parse()
    mask_folder = Path(args.mask_folder).resolve()
    files_path = list(mask_folder.glob('*.png'))
    if files_path: 
        anno_folder = Path.joinpath(mask_folder.parent, 'annos')
        Path(anno_folder).mkdir(parents=True, exist_ok=True)
        for f in files_path:
            anno_filename = Path.joinpath(anno_folder, Path(f).stem + '.csv')
            with open(anno_filename, 'w') as anno_file:
                size, polygons = get_polygon(f, show_img=False)
                annos = get_annos(f, size, polygons)
                for anno in annos:
                    anno_file.write(anno+'\n')
                    print(f'{anno_filename} has been saved...')

    else:
        print('E: Make sure you have given the right mask folder...')
        sys.exit()
        

if __name__=='__main__':
    main()
