import os
import sys
import random
import numpy as np
import matplotlib.pyplot as plt
from train import CustomDataset, CustomConfig

# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
from mrcnn.model import log

MODEL_DIR = os.path.join(ROOT_DIR, "logs")


class InferenceConfig(CustomConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


def get_ax(rows=1, cols=2, size=8):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.
    
    Change the default size attribute to control the size
    of rendered images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    return ax


def main():
    data_set = "aqua"
    inference_config = InferenceConfig()
    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                              config=inference_config,
                              model_dir=MODEL_DIR)
    
    # Get path to saved weights
    # Either set a specific path or find last trained weights
    # model_path = os.path.join(ROOT_DIR, ".h5 file name here")
    model_path = model.find_last()
    #model_path = './logs/shapes20200503T2218/mask_rcnn_shapes_0037.h5'
    
    # Load trained weights
    print("Loading weights from ", model_path)
    model.load_weights(model_path, by_name=True)
    
    # Train dataset
    dataset_train = CustomDataset()
    dataset_train.load_custom(f"./{data_set}/", "train")
    dataset_train.prepare()
    # Validation dataset
    dataset_val = CustomDataset()
    dataset_val.load_custom(f"./{data_set}/", "val")
    dataset_val.prepare()
    
    # If you only want to test on a random image,
    # use the following line to choose image_id.
    #image_id = random.choice(dataset_val.image_ids)
    APs = []
    for image_id in dataset_val.image_ids:
        # Show ground truth
        original_image, image_meta, gt_class_id, gt_bbox, gt_mask = \
                modellib.load_image_gt(dataset_val, inference_config, 
                        image_id, use_mini_mask=False)
        
        log("original_image", original_image)
        log("image_meta", image_meta)
        log("gt_class_id", gt_class_id)
        log("gt_bbox", gt_bbox)
        log("gt_mask", gt_mask)
        
        ax = get_ax()
        visualize.display_instances(
                original_image, gt_bbox, gt_mask, gt_class_id, 
                dataset_train.class_names, ax=ax[0],
                title=f'Original {data_set} w/ masks')
        
        # Do detection
        results = model.detect([original_image], verbose=1)
        r = results[0]
        visualize.display_instances(
                original_image, r['rois'], r['masks'], r['class_ids'], 
                dataset_val.class_names, r['scores'],ax=ax[1],
                title=f'Detected {data_set} w/ masks')
        plt.show()

        # Compute AP
        AP, precisions, recalls, overlaps =\
            utils.compute_ap(gt_bbox, gt_class_id, gt_mask,
                             r["rois"], r["class_ids"], r["scores"], r['masks'])
        APs.append(AP)
        
    print("mAP: ", np.mean(APs))


if __name__=='__main__':
    main()
